<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'Kudukaya' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '@%=|MAr0=OSQkcV9IERzqOsGv>#AhiD^7!<Ns;Iely(Ux^bn/<8Sw@!XOGN@A~.e' );
define( 'SECURE_AUTH_KEY',  'h(=&c[.ADh0iX(%kF9S>L|d_tpTke`T^D sw%[<_Kpqv!&SYap+n:Ot>9yPa+uNd' );
define( 'LOGGED_IN_KEY',    '{%+fP<m>mdMGEaK4SjsCA@Cpky)%kTQW[}&yC (/BV~jWf!qbXN7af*0c{zu>]v#' );
define( 'NONCE_KEY',        '/^`p5qlD|4:T-#xC:{poi@GXs(UMTAJv_^BuCp}:)2Q2xkV#jV.BX0qk)FOjt/)O' );
define( 'AUTH_SALT',        'p&xW]b9_ .0MyK5I8,>`FM5?]GHY@?wV}TSGH w7,ho@Z5zOlDJ]&s ,zQB9|x#O' );
define( 'SECURE_AUTH_SALT', 'j(r[~p@6w@u+Eyy;U#*s]Fvh#)@9^>YW?y&HGW/I*3x@*Y0D[G{B ,6LNPzkoa#)' );
define( 'LOGGED_IN_SALT',   '0euUwEln{f*`f92fJ,x.):#I(za{^xm&.$mxa&Q23]:PKUE!gxgouFve_i42mVME' );
define( 'NONCE_SALT',       'pe:;;Uit#VpLHY`crS-V4`B8`&}.>Y*`R,xA2UE3tq?3GHm w.VIQ`ky!a~WK^*>' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
